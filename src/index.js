import React from 'react';
import { render } from 'react-dom';
import ProductsList from './Products/ProductsList';

render(<ProductsList />, document.getElementById("root"));