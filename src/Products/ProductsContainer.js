import React, { Component } from 'react'
import Product from './Product';

export default class ProductsContainer extends Component {
    constructor(props) {
        super(props)

        this.state = {
            products: null,
            showSingleView: false,
            product: null,
            backToListText: 'Visa alla produkter'
        }
    }

    componentDidMount() {
        this.getJsonData()  
        .then((data) => {
            let productsObject = this.createProducts(data)
            let productsElement = this.props.render(productsObject)
            this.setState({products: productsElement})        
        })
    }

    getJsonData() {
        return fetch('data.json')
            .then((res) => {
                return res.json()
            })
    }

    createProducts(data) {
        let productsArray = []
        for(let i = 0; i < data.items.length; i++) {
            let firstSentence = this.getFirstSentence(data.items[i].description)
            let product = {
                id: data.items[i].productId,
                description: this.filterHtmlTags(firstSentence),
                longDescription: this.filterHtmlTags(data.items[i].description),
                name: data.items[i].variantInfos[0].name,
                price: data.items[i].variantInfos[0].price.recommendedPriceInclVat,
                priceType: data.items[i].variantInfos[0].price.currency,
                createProduct: this.createProduct.bind(this),
                showSingleView: this.state.showSingleView,
                key: data.items[i].productId
            }
            productsArray.push(product)
        }    
        return productsArray            
    }

    getFirstSentence(text) { 
        let index = text.indexOf('.') + 1
        return text.slice(0, index)  
    }

    filterHtmlTags(html) {
        let div = document.createElement('div')
        div.innerHTML = html
        return div.innerText || div.textContent
    }

    createProduct(id) {
        let allProducts = this.state.products
        let product
        for(let i = 0; i < allProducts.length; i++) {
            if(allProducts[i].props.id === id) {
                product = {
                    id: allProducts[i].props.id,
                    name: allProducts[i].props.name,
                    description: allProducts[i].props.longDescription,
                    price: allProducts[i].props.price,
                    priceType: allProducts[i].props.priceType,
                    backToListText: this.state.backToListText,
                    backToList: this.backToListView.bind(this),
                    showSingleView: this.state.showSingleView,
                    createProduct: this.createProduct.bind(this)
                }
            }
        }
        this.setStateToSingleView(product)
    }

    setStateToSingleView(product) {  
        this.setState({product: product})
        this.setState({showSingleView: true})
    }

    backToListView() {
        this.setState({showSingleView: false})
    }

    render() { 
        if(!this.state.showSingleView) {
            return(
                <div id='container'>
                    {this.state.products}
                </div>
            )
        } 
        return(            
            <div>
                <Product {...this.state.product}/>
            </div>
        )      
    }
}