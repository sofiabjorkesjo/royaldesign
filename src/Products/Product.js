import React, { Component } from 'react'
import styled from 'react-emotion'

const ProductBox = styled('div')`
  background: #f0f0f0;
  margin: 20px;
  padding: 20px;
  box-shadow: 2px 3px 5px #D5D5D6;
  cursor: pointer;  
`
const BackToList = styled('span')`
    cursor: pointer;
    color: #000000; 
    font-size: 20px;
    font-family: Verdana;
`

const H1 = styled('h1')`
  color: #6FA8EC; 
  font-size: 25px;
  font-family: Verdana;
`

const Price = styled('p')`
  color: #F8031C; 
  font-size: 10x;
  font-family: Verdana;
`

export default class Product extends Component {

    onClickProduct() {
        this.props.createProduct(this.props.id)        
    }
    
    render() {
        return (
            <div id={this.props.id}>
                <BackToList onClick={this.props.backToList}>{this.props.backToListText}</BackToList>
                <ProductBox onClick={this.onClickProduct.bind(this)}>
                    <H1>{this.props.name}</H1>
                    <p>{this.props.description}</p>
                    <Price>{this.props.price}  {this.props.priceType}</Price>
                </ProductBox>
            </div>  
        )     
    }
}
