import React, { Component } from 'react'
import ProductsContainer from './ProductsContainer'
import Product from './Product'
import styled from 'react-emotion'


export default class ProductsList extends Component {
     
    render() {

        return <div>
              
            {/* This line is not allowed to be modified or changed */}
            <ProductsContainer render={products => products.map(product => <Product {...product}/>)} />
            {/* you are allowed to modify as you see fit below this point */}
             
        </div>

        
    }

}

