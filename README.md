# Products List

### RoyalDesign.se dev assignment
Hey! We think it is really cool that you want to come work with us. 🙌
To get a better understanding of what you can do we have a small assignment for you.

Start by cloning this repository and create your own repository on Bitbucket and push your code there.
Give us read-access to your repository so we can watch your commits.

### Deadline
From the day you got access to this repository you have two weeks to finish. We will check out the latest commit before the deadline. 

### Project
The provided project is a basic React project bundled with webpack. Start the project by running `yarn start`.
The project follows a container approach and this should not be changed. See comments in the code what you are not allowed to change.

### CSS
Should be handled with emotion. It's prepacked and ready to use in the components.

### Goal
Render a view of products.

### Minimium requirements
The view should display a grid or list of products defined in the data file. Name, a shorter description and price shall be displayed.
The products do not have to be clickable or any other functionality added.

### Taking it to the next level
Make the products clickable and display a single view for each product.
Display more of the available properties as you see fit in the grid and/or the single product view.
Fix errors if they are any, improve the project structure. What else can be done? 

### What we look at
* Commits
* Code style
* Design

and many more details, just code as you normally do and it will surely go fine. 🙂

**Good luck!**