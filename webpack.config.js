const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin')
const HtmlWebPackPlugin = require("html-webpack-plugin")

module.exports = {
	entry: './src/index.js',
	devServer: {
		port: 8080,
		contentBase: path.join(__dirname, 'dist')
	},
  	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: "babel-loader",
					options: {
						presets: ['react']
					}
				}
			},
			{
				test: /\.html$/,
				use: [
					{
						loader: "html-loader"
					}
				]
			}
		]
	},
	plugins: [
		new HtmlWebPackPlugin({
			template: "./src/index.html",
			filename: "./index.html"
		}),
		new CopyWebpackPlugin([{ from: 'data.json', to: 'data.json' }])
	  ]
}